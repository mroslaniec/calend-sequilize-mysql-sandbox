const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
  const PaymentGatewaySetting = sequelize.define('paymentGatewaySetting', {
    currency: {
      type: DataTypes.STRING(3), allowNull: false
    },
    paymentGateway: {
      type: DataTypes.STRING(30), allowNull: false
    },
    setting: {
      type: DataTypes.JSON, allowNull: false
    }
  }, { tableName: 'paymentGatewaySetting' });

  return PaymentGatewaySetting;
};
