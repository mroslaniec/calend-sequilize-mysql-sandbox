const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
  const BookingEventType = sequelize.define('bookingEventType', {
    name: {
      type: DataTypes.STRING(30), allowNull: false
    },
    meetingType: {
      type: DataTypes.STRING(30), allowNull: false
    },
    duration: {
      type: DataTypes.INTEGER, allowNull: false
    },
    location: {
      type: DataTypes.STRING(50)
    },
    color: {
      type: DataTypes.STRING(7)
    },
    description: {
      type: DataTypes.TEXT('small')
    },
    maxTimeToCancelBeforeEvent: {
      type: DataTypes.INTEGER
    },
    slug: {
      type: DataTypes.STRING(30)
    }
    
  }, { tableName: 'bookingEventType' });

  return BookingEventType;
};