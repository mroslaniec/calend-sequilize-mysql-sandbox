const { DataTypes, Sequelize } = require('sequelize');

module.exports = (sequelize) => {
  const ScheduleOverride = sequelize.define('scheduleOverride', {
    overridenDate: {
      type: DataTypes.DATEONLY, allowNull: false
    },
    timezone: {
      type: DataTypes.STRING(30), allowNull: false
    },
    startHour: {
      type: Sequelize.TIME, allowNull: false
    },
    endHour: {
      type: Sequelize.TIME,  allowNull: false
    }
  }, { tableName: 'scheduleOverride' });

  return ScheduleOverride;
};
