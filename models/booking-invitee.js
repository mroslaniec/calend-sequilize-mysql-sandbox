const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
  const BookingInvitee = sequelize.define('bookingInvitee', {
    fullName: {
      type: DataTypes.STRING(50), allowNull: false
    },
    email: {
      type: DataTypes.STRING(50)
    },
    phoneNumber: {
      type: DataTypes.STRING (15)
    }
  }, { tableName: 'bookingInvitee' });

  return BookingInvitee;
};
