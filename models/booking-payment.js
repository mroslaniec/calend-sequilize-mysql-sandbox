const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
  const BookingPayment = sequelize.define('bookingPayment', {
    uuid: {
      type: DataTypes.UUID, allowNull: false
    },
    amount: {
      type: DataTypes.DECIMAL, allowNull: false
    },
    currency: {
      type: DataTypes.STRING(3), allowNull: false
    },
    transactionNo: {
      type: DataTypes.STRING(50), allowNull: false
    },
    transactionSession: {
      type: DataTypes.STRING(50), allowNull: false
    },
    transactionStatus: {
      type: DataTypes.STRING(20), allowNull: false
    }
  }, { tableName: 'bookingPayment' });

  return BookingPayment;
};