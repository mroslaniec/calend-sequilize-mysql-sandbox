const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
  const EventType = sequelize.define('eventType', {
    name: {
      type: DataTypes.STRING(30), allowNull: false
    },
    meetingType: {
      type: DataTypes.STRING(30), allowNull: false
    },
    duration: {
      type: DataTypes.INTEGER, allowNull: false
    },
    location: {
      type: DataTypes.STRING(50)
    },
    color: {
      type: DataTypes.STRING(7)
    },
    description: {
      type: DataTypes.STRING
    },
    price: {
      type: DataTypes.DECIMAL
    },
    currency: {
      type: DataTypes.STRING(3)
    },
    maxTimeToCancelBeforeEvent: {
      type: DataTypes.INTEGER
    },
    slug: {
      type: DataTypes.STRING(30), allowNull: false
    }
    
  }, { tableName: 'eventType' });

  return EventType;
};
