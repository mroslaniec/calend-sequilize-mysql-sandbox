const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
  const Booking = sequelize.define('booking', {
    uuid: {
      type: DataTypes.UUID, allowNull: false
    },
    startTime: {
      type: DataTypes.DATE, allowNull: false
    },
    endTime: {
      type: DataTypes.DATE, allowNull: false
    },
    status: {
      type: DataTypes.STRING(20), allowNull: false
    },
    price: {
      type: DataTypes.DECIMAL
    },
    currency: {
      type: DataTypes.STRING(3)
    },
    paid: {
      type: DataTypes.BOOLEAN
    },
    inviteeNote: {
      type: DataTypes.TEXT('small')
    },
    cancelledAt: {
      type: DataTypes.DATE
    },
    rescheduledBy: {
      type: DataTypes.STRING(20)
    },
    rescheduledNote: {
      type: DataTypes.TEXT('small')
    }
  }, { tableName: 'booking' });

  return Booking;
};
