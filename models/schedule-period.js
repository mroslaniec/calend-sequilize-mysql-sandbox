const { DataTypes, Sequelize } = require('sequelize');

module.exports = (sequelize) => {
  const SchedulePeriod = sequelize.define('schedulePeriod', {
    weekday: {
      type: DataTypes.INTEGER, allowNull: false
    },
    startHour: {
      type: Sequelize.TIME, allowNull: false
    },
    endHour: {
      type: Sequelize.TIME, allowNull: false
    }
  }, { tableName: 'schedulePeriod' });

  return SchedulePeriod;
};
