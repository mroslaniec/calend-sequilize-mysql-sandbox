const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
  const Schedule = sequelize.define('schedule', {
    startsAt: {
      type: DataTypes.DATEONLY, allowNull: false
    },
    endsAt: {
      type: DataTypes.DATEONLY
    },
    timezone: {
      type: DataTypes.STRING(30), allowNull: false
    },
    name: {
      type: DataTypes.STRING(30), allowNull: false
    }
  }, { tableName: 'schedule' });

  return Schedule;
};
