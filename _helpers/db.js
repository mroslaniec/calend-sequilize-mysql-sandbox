const config = require('config.json');
const mysql = require('mysql2/promise');
const { Sequelize } = require('sequelize');

module.exports = db = {};

initialize();

async function initialize() {
    // create db if it doesn't already exist
    const { host, port, user, password, database } = config.database;
    const connection = await mysql.createConnection({ host, port, user, password });
    await connection.query(`CREATE DATABASE IF NOT EXISTS \`${database}\`;`);

    // connect to db
    const sequelize = new Sequelize(database, user, password, { dialect: 'mysql' });

    // init models and add them to the exported db object
    db.Account = require('../accounts/account.model')(sequelize);
    db.RefreshToken = require('../accounts/refresh-token.model')(sequelize);
    
    db.Schedule = require('../models/schedule.js')(sequelize);
    db.ScheduleOverride = require('../models/schedule-override.js')(sequelize);
    db.SchedulePeriod = require('../models/schedule-period.js')(sequelize);
    db.EventType = require('../models/event-type.js')(sequelize);
    db.Booking = require('../models/booking.js')(sequelize);
    db.BookingEventType = require('../models/booking-event-type.js')(sequelize);
    db.BookingInvitee = require('../models/booking-invitee.js')(sequelize);
    db.BookingPayment = require('../models/booking-payment.js')(sequelize);
    db.PaymentGatewaySetting = require('../models/payment-gateway-setting.js')(sequelize);

    // define relationships
    db.Account.hasMany(db.RefreshToken, { onDelete: 'CASCADE' });
    db.RefreshToken.belongsTo(db.Account);

    db.Schedule.belongsTo(db.Account);
    db.Schedule.hasMany(db.SchedulePeriod, {
      as: 'periods', onDelete: 'CASCADE'
    });
    db.Schedule.belongsToMany(db.EventType, {
      through: 'scheduleEventType',
      as: 'eventTypes',
      onDelete: 'CASCADE'
    });

    db.ScheduleOverride.belongsTo(db.Account);
    db.EventType.belongsTo(db.Account);
    db.Booking.belongsTo(db.Account);
    db.Booking.belongsTo(db.BookingEventType);
    db.Booking.belongsTo(db.BookingInvitee);
    db.Booking.belongsTo(db.BookingPayment);
    db.BookingInvitee.hasOne(db.Booking);

    db.PaymentGatewaySetting.belongsTo(db.Account);

    
    // sync all models with database
    await sequelize.sync();
}